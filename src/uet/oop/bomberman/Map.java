package uet.oop.bomberman;

import uet.oop.bomberman.entities.*;
import uet.oop.bomberman.entities.abstracts.Entity;
import uet.oop.bomberman.entities.bomber.Bomber;
import uet.oop.bomberman.entities.enemies.*;
import uet.oop.bomberman.graphics.Sprite;
import uet.oop.bomberman.helpers.Lib;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;


public class Map  {
    public static int level = 1;
    public static int WIDTH = 105;
    public static int HEIGHT = 105;

    public static final ArrayList<Integer>[][] collidingList = new ArrayList[HEIGHT * Sprite.SCALED_SIZE][WIDTH * Sprite.SCALED_SIZE];

    public static final Entity[][] objectMap = new Entity[HEIGHT][WIDTH];

    public static Entity getObjectAt(int i, int j) {
        return objectMap[i][j];
    }

    public static void setObjectAt(int i, int j, Entity e) {
        objectMap[i][j] = e;
    }

    public static void initializeCollidingList() {
        int numRow = HEIGHT * Sprite.SCALED_SIZE;
        int numCol = WIDTH * Sprite.SCALED_SIZE;
        for (int i = 0; i < numRow; ++i) {
            for (int j = 0; j < numCol; ++j) {
                collidingList[i][j] = new ArrayList();
                for (int k = 0; k < HEIGHT; ++k) {
                    for (int h = 0; h < WIDTH; ++h) {
                        if (Lib.isColliding(i, j, k * Sprite.SCALED_SIZE, h * Sprite.SCALED_SIZE)) {
                            collidingList[i][j].add(k * WIDTH + h);
                        }
                    }
                }
            }
        }
    }

    public static boolean moveAble(int x, int y) {
        for(int id: collidingList[x][y]) {
            if (!(getObjectAt(id / WIDTH, id % WIDTH) instanceof Grass)) {
                return false;
            }
        }
        return true;
    }

    public static void readMap() {
        Scanner scanner = null;
        try {
            scanner = new Scanner(new File(String.format("res/levels/Level%d.txt", level)));
        } catch (FileNotFoundException err) {
            BombermanGame.victoryScene();
            return;
        }
        Container.clear();
        int levelId = scanner.nextInt();
        int numRow = scanner.nextInt();
        int numCol = scanner.nextInt();

        // skip a line
        scanner.nextLine();
        for (int i = 0; i < numRow; ++i) {
            String s = scanner.nextLine();
            for (int j = 0; j < numCol; ++j) {
                Entity object;
                switch (s.charAt(j)) {
                    case '#' -> {
                        object = new Wall(j, i, Sprite.wall.getFxImage());
                        Container.stillObjects.add(object);
                    }
                    case 'f', 'b', 's', 'x' -> {
                        object = new Brick(j, i, Sprite.brick.getFxImage());
                        Item.ITEM_TYPE type = Item.ITEM_TYPE.FLAME_ITEM;
                        switch (s.charAt(j)) {
                            case 'b' -> type = Item.ITEM_TYPE.BOMB_ITEM;
                            case 's' -> type = Item.ITEM_TYPE.SPEED_ITEM;
                            case 'x' -> type = Item.ITEM_TYPE.PORTAL_ITEM;
                        }
                        Container.items.add(new Item(j, i, type));
                        Container.bricks.add((Brick) object);
                        Container.stillObjects.add(new Grass(j, i, Sprite.grass.getFxImage()));
                    }
                    case '*' -> {
                        Container.stillObjects.add(new Grass(j, i, Sprite.grass.getFxImage()));
                        object = new Brick(j, i, Sprite.brick.getFxImage());
                        Container.bricks.add((Brick) object);
                    }
                    case 'p' -> {
                        Container.bomberman = new Bomber(j, i, Sprite.player_right.getFxImage());
                        object = new Grass(j, i, Sprite.grass.getFxImage());
                        Container.stillObjects.add(object);
                        Container.entities.add(Container.bomberman);
                    }
                    case '1' -> {
                        Balloon balloon = new Balloon(j, i, null);
                        object = new Grass(j, i, Sprite.grass.getFxImage());
                        Container.stillObjects.add(object);
                        Container.enemies.add(balloon);
                    }
                    case '2' -> {
                        Oneal oneal = new Oneal(j, i, null);
                        object = new Grass(j, i, Sprite.grass.getFxImage());
                        Container.stillObjects.add(object);
                        Container.enemies.add(oneal);
                    }
                    case '3' -> {
                        Kondoria kondoria = new Kondoria(j, i, null);
                        object = new Grass(j, i, Sprite.grass.getFxImage());
                        Container.stillObjects.add(object);
                        Container.enemies.add(kondoria);
                    }
                    case '4' -> {
                        Doll doll = new Doll(j, i, null);
                        object = new Grass(j, i, Sprite.grass.getFxImage());
                        Container.stillObjects.add(object);
                        Container.enemies.add(doll);
                    }
                    case '5' -> {
                        Minvo minvo = new Minvo(j, i, null);
                        object = new Grass(j, i, Sprite.grass.getFxImage());
                        Container.stillObjects.add(object);
                        Container.enemies.add(minvo);
                    }
                    default -> {
                        object = new Grass(j, i, Sprite.grass.getFxImage());
                        Container.stillObjects.add(object);
                    }
                }
                setObjectAt(j, i, object);
            }
        }

        HEIGHT = numCol;
        WIDTH = numRow;
        initializeCollidingList();
    }

    public static void levelUp() {
        level = level + 1;
        readMap();
    }
}

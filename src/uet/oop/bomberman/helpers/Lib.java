package uet.oop.bomberman.helpers;

import uet.oop.bomberman.Container;
import uet.oop.bomberman.Map;
import uet.oop.bomberman.entities.Grass;
import uet.oop.bomberman.entities.abstracts.MovingEntity;
import uet.oop.bomberman.graphics.Sprite;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Queue;

public class Lib {
    public static final int DELTA = 4;

    private static int sizeQueue;

    private static int timer = 1;
    private static final int[][] lastVisited = new int[105][105];
    public static MovingEntity.MOVING_DIRECTION[][] bestDirection = new MovingEntity.MOVING_DIRECTION[105][105];

    private static int calcSharedLength(int a, int a1) {
        // a and a1 should be in range [a + DELTA, a + Sprite.SCALE_Size - DELTA]
        a = a + DELTA;
        a1 = a1 + DELTA;
        return Math.max(0, Math.min(a, a1) + Sprite.SCALED_SIZE - DELTA - Math.max(a, a1));
    }

    private static int calcSharedArea(int x, int y, int x1, int y1) {
        return calcSharedLength(x, x1) * calcSharedLength(y, y1);
    }

    public static boolean isColliding(int x, int y, int x1, int y1) {
        return calcSharedArea(x, y, x1, y1) > 0;
    }

    private static int calcSharedLengthWithoutDelta(int a, int a1) {
        return Math.max(0, Math.min(a, a1) + Sprite.SCALED_SIZE - Math.max(a, a1));
    }

    private static int calcSharedAreaWithoutDelta(int x, int y, int x1, int y1) {
        return calcSharedLengthWithoutDelta(x, x1) * calcSharedLengthWithoutDelta(y, y1);
    }

    public static boolean isTouching(int x, int y, int x1, int y1) {
        return calcSharedAreaWithoutDelta(x, y, x1, y1) > DELTA * Sprite.SCALED_SIZE;
    }

    public static int distanceToBomberman(int x, int y) {
        return Math.abs(x - Container.bomberman.getX()) + Math.abs(y - Container.bomberman.getY());
    }

    public static void doBfs(int x, int y) {
        timer++;
        lastVisited[x][y] = timer;
        bestDirection[x][y] = MovingEntity.MOVING_DIRECTION.STAND;
        Queue<Integer> queue = new ArrayDeque<>();
        queue.add(x * Map.WIDTH + y);
        while(!queue.isEmpty()) {
            int cx = queue.peek() / Map.WIDTH;
            int cy = queue.peek() % Map.WIDTH;
            queue.remove();
            for (MovingEntity.MOVING_DIRECTION direction: MovingEntity.MOVING_DIRECTION.values()) {
                if (direction == MovingEntity.MOVING_DIRECTION.STAND) continue;
                int nx = cx;
                int ny = cy;
                switch (direction) {
                    case UP -> ny += 1;
                    case DOWN -> ny -= 1;
                    case RIGHT -> nx -= 1;
                    case LEFT -> nx += 1;
                }
                if (nx < 0 || nx >= Map.HEIGHT || ny < 0 || ny >= Map.WIDTH || !(Map.getObjectAt(nx, ny) instanceof Grass)) continue;
                if (lastVisited[nx][ny] < timer) {
                    lastVisited[nx][ny] = timer;
                    bestDirection[nx][ny] = direction;
                    queue.add(nx * Map.WIDTH + ny);
                }
            }
        }
    }

    public static boolean hasPathToBomber(int x, int y) {
        return lastVisited[x][y] >= timer;
    }

    public static MovingEntity.MOVING_DIRECTION getBestDirection(int x, int y) {
        if (!hasPathToBomber(x, y)) return null;
        return bestDirection[x][y];
    }
}

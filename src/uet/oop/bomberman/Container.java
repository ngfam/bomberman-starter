package uet.oop.bomberman;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import uet.oop.bomberman.entities.*;
import uet.oop.bomberman.entities.abstracts.AutoMovingEnemy;
import uet.oop.bomberman.entities.abstracts.Entity;
import uet.oop.bomberman.entities.abstracts.MovingEntity;
import uet.oop.bomberman.entities.bomber.Bomb;
import uet.oop.bomberman.entities.bomber.Bomber;
import uet.oop.bomberman.entities.bomber.Explosion;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;

public class Container {
    public static Bomber bomberman;
    public static List<MovingEntity> entities = new ArrayList<>();
    public static List<Entity> stillObjects = new ArrayList<>(); // Grass & Wall
    public static List<Brick> bricks = new ArrayList<>();
    public static List<Bomb> bombs = new ArrayList<>();
    public static List<Explosion> explosions = new ArrayList<>();
    public static List<Item> items = new ArrayList<>();
    public static List<AutoMovingEnemy> enemies = new ArrayList<>();

    public static void clear() {
        bomberman = null;
        entities.clear();
        stillObjects.clear();
        bombs.clear();
        bricks.clear();
        explosions.clear();
        items.clear();
        enemies.clear();
    }

    public static void update() {
        try {
            // update
            entities.forEach(Entity::update);
            bombs.forEach(Entity::update);
            explosions.forEach(Entity::update);
            bricks.forEach(Entity::update);
            items.forEach(Entity::update);
            enemies.forEach(Entity::update);
            // remove
            bombs.removeIf(Bomb::fullyExploded);
            bricks.removeIf(Brick::isFullyDestroyed);
            items.removeIf(Item::isEaten);
            explosions.removeIf(Explosion::doneExploding);
            entities.removeIf(MovingEntity::isFullyDead);
            enemies.removeIf(MovingEntity::isFullyDead);
        } catch (ConcurrentModificationException ignored) {
        }
    }

    public static void render(GraphicsContext gc, Canvas canvas) {
        gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        stillObjects.forEach(g -> g.render(gc));
        items.forEach(g -> g.render(gc));
        bricks.forEach(g -> g.render(gc));
        bombs.forEach(g -> g.render(gc));
        entities.forEach(g -> g.render(gc));
        enemies.forEach(g -> g.render(gc));
        explosions.forEach(g -> g.render(gc));
    }

    public static boolean isGameOver() {
        return bomberman.isFullyDead();
    }
}

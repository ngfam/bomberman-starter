package uet.oop.bomberman;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import uet.oop.bomberman.entities.abstracts.MovingEntity;
import uet.oop.bomberman.entities.bomber.Bomb;
import uet.oop.bomberman.graphics.Sprite;
import uet.oop.bomberman.helpers.Lib;
import uet.oop.bomberman.helpers.SoundPlayer;

import javax.sound.sampled.Clip;
import java.awt.*;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class BombermanGame extends Application {
    public static GraphicsContext gc;
    public static Canvas canvas;
    public static Scene scene;
    public static Group root;
    public static Stage gameStage;
    public static AnimationTimer timer;

    public static void main(String[] args) {
        SoundPlayer backgroundMusicPlayer = new SoundPlayer("sounds/background.wav", Clip.LOOP_CONTINUOUSLY, 10);
        Map.readMap();
        Application.launch(BombermanGame.class);
    }

    @Override
    public void start(Stage stage) {
        // Tao Canvas
        gameStage = stage;
        canvas = new Canvas(Sprite.SCALED_SIZE * Map.HEIGHT, Sprite.SCALED_SIZE * Map.WIDTH);
        gc = canvas.getGraphicsContext2D();

        // Tao root container
        root = new Group();
        root.getChildren().add(canvas);

        // Tao scene
        scene = new Scene(root);
        scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
              @Override
              public void handle(KeyEvent keyEvent) {
                  switch (keyEvent.getCode()) {
                      case SPACE -> {
                          if (Container.bombs.size() == Container.bomberman.getBombLimit()) {
                              return;
                          }
                          int x = Container.bomberman.getGridX();
                          int y = Container.bomberman.getGridY();
                          Bomb bomb = new Bomb(x, y, Sprite.bomb.getFxImage(), Container.bomberman.getBombLength());
                          Container.bombs.add(bomb);
                      }
                      case LEFT -> {
                          Container.bomberman.setMoving(true);
                          Container.bomberman.setMovingDirection(MovingEntity.MOVING_DIRECTION.LEFT);
                      }
                      case RIGHT -> {
                          Container.bomberman.setMoving(true);
                          Container.bomberman.setMovingDirection(MovingEntity.MOVING_DIRECTION.RIGHT);
                      }
                      case UP -> {
                          Container.bomberman.setMoving(true);
                          Container.bomberman.setMovingDirection(MovingEntity.MOVING_DIRECTION.UP);
                      }
                      case DOWN -> {
                          Container.bomberman.setMoving(true);
                          Container.bomberman.setMovingDirection(MovingEntity.MOVING_DIRECTION.DOWN);
                      }
                  }
              }
        });

        scene.setOnKeyReleased(new EventHandler<KeyEvent>() {
           @Override
           public void handle(KeyEvent keyEvent) {
               switch (keyEvent.getCode()) {
                   case LEFT, RIGHT, UP, DOWN -> Container.bomberman.setMoving(false);
               }
           }
       });

        // Them scene vao stage
        stage.setScene(scene);
        stage.show();


        timer = new AnimationTimer() {
            int countdownBfs = 0;
            @Override
            public void handle(long l) {
                if (countdownBfs == 0) {
                    Lib.doBfs(Container.bomberman.getGridX(), Container.bomberman.getGridY());
                    countdownBfs = 30;
                } else {
                    countdownBfs--;
                }
                render();
                if (update(stage)) {
                    defeatedScene();
                }
            }
        };
        timer.start();
    }


    private boolean update(Stage stage) {
        Container.update();
        return Container.isGameOver();
    }

    public void render() {
        Container.render(gc, canvas);
    }

    private static void endingScene(String imagePath) {
        timer.stop();
        try {
            InputStream stream = new FileInputStream(imagePath);
            javafx.scene.image.Image image = new Image(stream);
            ImageView view = new ImageView();
            view.setImage(image);
            view.setFitHeight(canvas.getHeight());
            view.setFitWidth(canvas.getWidth());
            gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
            root = new Group(view);
            root.getChildren().add(canvas);
            scene = new Scene(root);
            scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
                @Override
                public void handle(KeyEvent keyEvent) {
                    System.exit(0);
                }
            });
            gameStage.setScene(scene);
            gameStage.show();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.exit(0);
        }
    }

    public static void defeatedScene() {
        endingScene("res/images/defeated.jpg");
    }

    public static void victoryScene() {
        endingScene("res/images/victory.jpg");
    }
}

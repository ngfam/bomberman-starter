package uet.oop.bomberman.entities.bomber;

import javafx.scene.image.Image;
import uet.oop.bomberman.Container;
import uet.oop.bomberman.entities.abstracts.Entity;
import uet.oop.bomberman.entities.abstracts.MovingEntity;
import uet.oop.bomberman.graphics.Sprite;
import uet.oop.bomberman.helpers.SoundPlayer;

public class Bomber extends MovingEntity {
    private int bombLength = 2;
    private int bombLimit = 1;


    public Bomber(int x, int y, Image img) {
        super(x, y, img);
        this.setMoving(false);
        this.setMovingDirection(MOVING_DIRECTION.RIGHT);
    }

    @Override
    public void update() {
        if (!this.isDead() && (checkTouchingExplosion() || checkTouchingEnemies())) {
            this.setDead(true);
        }
        if (this.isDead()) {
            this.updateImageDying();
        } else {
            this.updateImage();
        }
        this.animate();
    }

    @Override
    public void updateImage() {
        Sprite spriteToUpdate;
        switch(this.getMovingDirection()) {
            case LEFT -> {
                if (isMoving()) {
                    this.moveLeft();
                    spriteToUpdate = Sprite.movingSprite(Sprite.player_left_1, Sprite.player_left_2, this.getAnimatingStep(), ANIMATING_CYCLE);
                } else {
                    spriteToUpdate = Sprite.player_left;
                }
            }
            case RIGHT -> {
                if (isMoving()) {
                    this.moveRight();
                    spriteToUpdate = Sprite.movingSprite(Sprite.player_right_1, Sprite.player_right_2, this.getAnimatingStep(), ANIMATING_CYCLE);
                } else {
                    spriteToUpdate = Sprite.player_right;
                }
            }
            case UP -> {
                if (isMoving()) {
                    this.moveUp();
                    spriteToUpdate = Sprite.movingSprite(Sprite.player_up_1, Sprite.player_up_2, this.getAnimatingStep(), ANIMATING_CYCLE);
                } else {
                    spriteToUpdate = Sprite.player_up;
                }
            }
            case DOWN -> {
                if (isMoving()) {
                    this.moveDown();
                    spriteToUpdate = Sprite.movingSprite(Sprite.player_down_1, Sprite.player_down_2, this.getAnimatingStep(), ANIMATING_CYCLE);
                } else {
                    spriteToUpdate = Sprite.player_down;
                }
            }
            default -> {
                spriteToUpdate = Sprite.player_right;
            }
        }
        this.setImg(spriteToUpdate.getFxImage());
    }

    public void updateImageDying() {
        this.setImg(
            Sprite.movingSprite(
                    Sprite.player_dead1,
                    Sprite.player_dead2,
                    Sprite.player_dead3,
                    this.getAnimatingStep(),
                    Sprite.ANIMATING_CYCLE
            ).getFxImage()
        );
    }

    public int getBombLength() {
        return bombLength;
    }

    public void setBombLength(int bombLength) {
        this.bombLength = bombLength;
    }

    public int getBombLimit() {
        return bombLimit;
    }

    public void setBombLimit(int bombLimit) {
        this.bombLimit = bombLimit;
    }

    public void increaseBombLength() {
        this.setBombLength(this.bombLength + 1);
    }

    public void increaseBombLimit() {
        this.setBombLimit(this.bombLimit + 1);
    }

    public void increaseSpeed() {
        this.setMovingSpeed(this.movingSpeed + 1);
    }

    @Override
    public void setDead(boolean dead) {
        assert !this.dead && dead;
        this.dead = true;
        dyingCountdown = 60;
        SoundPlayer dyingSound = new SoundPlayer("sounds/die.wav", 0, 0);
    }

    public boolean checkTouchingEnemies() {
        for(Entity enemy: Container.enemies) {
            if (enemy.isTouching(this)) {
                return true;
            }
        }
        return false;
    }
}

package uet.oop.bomberman.entities.bomber;

import javafx.scene.image.Image;
import uet.oop.bomberman.Container;
import uet.oop.bomberman.Map;
import uet.oop.bomberman.entities.Brick;
import uet.oop.bomberman.entities.Wall;
import uet.oop.bomberman.entities.abstracts.Entity;
import uet.oop.bomberman.entities.bomber.Explosion;
import uet.oop.bomberman.graphics.Sprite;
import uet.oop.bomberman.helpers.SoundPlayer;

public class Bomb extends Entity {
    public static final int TICKING_CYCLE = 90;
    public static final int EXPLODING_CYCLE = 20;

    int length = 2;
    int animationStep;
    boolean exploded;

    public Bomb(int xUnit, int yUnit, Image img) {
        super(xUnit, yUnit, img);
        animationStep = 0;
    }

    public Bomb(int xUnit, int yUnit, Image img, int length) {
        super(xUnit, yUnit, img);
        animationStep = 0;
        this.length = length;
    }

    @Override
    public void update() {
        if (!this.isExploded() && this.checkTouchingExplosion()) {
            this.setExploded(true);
        }
        updateImage();
    }

    @Override
    public void updateImage() {
        if (!isExploded()) {
            setImg(
                    Sprite.movingSprite(
                            Sprite.bomb,
                            Sprite.bomb_1,
                            Sprite.bomb_2,
                            animationStep,
                            Sprite.ANIMATING_CYCLE
                    ).getFxImage()
            );
            animationStep = animationStep + 1;
            if (animationStep == TICKING_CYCLE) {
                this.setExploded(true);
            }
        }
        else {
            if (animationStep == EXPLODING_CYCLE) {
                return;
            }
            setImg(
                    Sprite.movingSprite(
                            Sprite.bomb_exploded,
                            Sprite.bomb_exploded1,
                            Sprite.bomb_exploded2,
                            animationStep,
                            Sprite.ANIMATING_CYCLE
                    ).getFxImage()
            );
            animationStep = animationStep + 1;
        }
    }

    public int getAnimationStep() {
        return animationStep;
    }

    public void setAnimationStep(int animationStep) {
        this.animationStep = animationStep;
    }

    public boolean isExploded() {
        return exploded;
    }

    public void setExploded(boolean exploded) {
        assert !this.exploded && exploded;
        SoundPlayer explodingSound = new SoundPlayer("sounds/explosion.wav", 0, 20);

        this.exploded = true;
        animationStep = 0;

        int sx = this.getGridX(), sy = this.getGridY();

        for(int i = 1; i < length; ++i) {
            int nx = sx;
            int ny = sy - i;
            if (explosionBlock(nx, ny)) break;;
            Container.explosions.add(
                    new Explosion(
                            nx,
                            ny,
                            null,
                            i == length - 1 ? Explosion.EXPLOSION_TYPE.TOP : Explosion.EXPLOSION_TYPE.VERTICAL
                    )
            );
        }

        for(int i = 1; i < length; ++i) {
            int nx = sx;
            int ny = sy + i;
            if (explosionBlock(nx, ny)) break;;
            Container.explosions.add(
                    new Explosion(
                            nx,
                            ny,
                            null,
                            i == length - 1 ? Explosion.EXPLOSION_TYPE.DOWN : Explosion.EXPLOSION_TYPE.VERTICAL
                    )
            );
        }

        for(int i = 1; i < length; ++i) {
            int nx = sx - i;
            int ny = sy;
            if (explosionBlock(nx, ny)) break;;
            Container.explosions.add(
                    new Explosion(
                            nx,
                            ny,
                            null,
                            i == length - 1 ? Explosion.EXPLOSION_TYPE.LEFT : Explosion.EXPLOSION_TYPE.HORIZONTAL
                    )
            );
        }

        for(int i = 1; i < length; ++i) {
            int nx = sx + i;
            int ny = sy;
            if (explosionBlock(nx, ny)) break;;
            Container.explosions.add(
                    new Explosion(
                            nx,
                            ny,
                            null,
                            i == length - 1 ? Explosion.EXPLOSION_TYPE.RIGHT : Explosion.EXPLOSION_TYPE.HORIZONTAL
                    )
            );
        }
    }

    private boolean explosionBlock(int x, int y) {
        Entity entity = Map.getObjectAt(x, y);
        if (entity instanceof Wall) {
            return true;
        }
        if (entity instanceof Brick) {
            ((Brick) entity).setDestroyed(true);
            return true;
        }
        return false;
    }

    public boolean fullyExploded() {
        return isExploded() && animationStep == EXPLODING_CYCLE;
    }
}

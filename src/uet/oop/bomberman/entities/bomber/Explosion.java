package uet.oop.bomberman.entities.bomber;

import javafx.scene.image.Image;
import uet.oop.bomberman.entities.abstracts.Entity;
import uet.oop.bomberman.graphics.Sprite;
import uet.oop.bomberman.helpers.Lib;

public class Explosion extends Entity {
    enum EXPLOSION_TYPE {
        VERTICAL,
        HORIZONTAL,
        TOP,
        DOWN,
        LEFT,
        RIGHT
    }

    EXPLOSION_TYPE type;
    int animationStep;
    public static int EXPLOSION_TIME = 20;
    public static int EXPLOSION_CYCLE = EXPLOSION_TIME / 3;


    public Explosion(int xUnit, int yUnit, Image img, EXPLOSION_TYPE type) {
        super(xUnit, yUnit, img);
        this.type = type;
    }

    @Override
    public void update() {
        updateImage();
    }

    @Override
    public void updateImage() {
        if (doneExploding()) return;
        setImg(
                getExplosionSprite().getFxImage()
        );
        animationStep++;
    }

    public boolean doneExploding() {
        return animationStep == EXPLOSION_TIME;
    }

    private Sprite getExplosionSprite() {
        switch (type) {
            case VERTICAL -> {
                return Sprite.movingSprite(Sprite.explosion_vertical, Sprite.explosion_vertical1, Sprite.explosion_vertical2, animationStep, EXPLOSION_CYCLE);
            }
            case HORIZONTAL -> {
                return Sprite.movingSprite(Sprite.explosion_horizontal, Sprite.explosion_horizontal1, Sprite.explosion_horizontal2, animationStep, EXPLOSION_CYCLE);
            }
            case TOP -> {
                return Sprite.movingSprite(Sprite.explosion_vertical_top_last, Sprite.explosion_vertical_top_last1, Sprite.explosion_vertical_top_last2, animationStep, EXPLOSION_CYCLE);
            }
            case DOWN -> {
                return Sprite.movingSprite(Sprite.explosion_vertical_down_last, Sprite.explosion_vertical_down_last1, Sprite.explosion_vertical_down_last2, animationStep, EXPLOSION_CYCLE);
            }
            case RIGHT -> {
                return Sprite.movingSprite(Sprite.explosion_horizontal_right_last, Sprite.explosion_horizontal_right_last1, Sprite.explosion_horizontal_right_last2, animationStep, EXPLOSION_CYCLE);
            }
            case LEFT -> {
                return Sprite.movingSprite(Sprite.explosion_horizontal_left_last, Sprite.explosion_horizontal_left_last1, Sprite.explosion_horizontal_left_last2, animationStep, EXPLOSION_CYCLE);
            }
            default -> {
                return null;
            }
        }
    }

    public boolean isTouching(Entity e) {
        return Lib.isTouching(this.getX(), this.getY(), e.getX(), e.getY());
    }
}

package uet.oop.bomberman.entities.enemies;

import javafx.scene.image.Image;
import uet.oop.bomberman.entities.abstracts.AutoMovingEnemy;
import uet.oop.bomberman.graphics.Sprite;
import uet.oop.bomberman.helpers.Lib;

import java.util.ArrayList;

public class Minvo extends AutoMovingEnemy {
    public Minvo(int xUnit, int yUnit, Image img) {
        super(xUnit, yUnit, img, 1, 4);
        this.left = Sprite.minvo_left1;
        this.left1 = Sprite.minvo_left2;
        this.left2 = Sprite.minvo_left3;
        this.right = Sprite.minvo_right1;
        this.right1 = Sprite.minvo_right2;
        this.right2 = Sprite.minvo_right3;
        this.deadSprite = Sprite.minvo_dead;

        movingAnimations[0] = this.left;
        movingAnimations[1] = this.left1;
        movingAnimations[2] = this.left2;
        this.setImg(this.left.getFxImage());
        this.setMovingSpeed(MIN_SPEED);
    }

    @Override
    protected MOVING_DIRECTION getBestDirection() {
        ArrayList<MOVING_DIRECTION> directions = getMovableDirections();
        MOVING_DIRECTION bfsDirection = Lib.getBestDirection(this.getGridX(), this.getGridY());
        if (bfsDirection != null && bfsDirection != MOVING_DIRECTION.STAND && directions.contains(bfsDirection)) {
            return bfsDirection;
        } else {
            return getRandomDirection();
        }
    }
}

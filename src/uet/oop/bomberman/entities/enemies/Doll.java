package uet.oop.bomberman.entities.enemies;

import javafx.scene.image.Image;
import uet.oop.bomberman.entities.abstracts.AutoMovingEnemy;
import uet.oop.bomberman.graphics.Sprite;

public class Doll extends AutoMovingEnemy {
    public Doll(int xUnit, int yUnit, Image img) {
        super(xUnit, yUnit, img, 2, 3);
        this.left = Sprite.doll_left1;
        this.left1 = Sprite.doll_left2;
        this.left2 = Sprite.doll_left3;
        this.right = Sprite.doll_right1;
        this.right1 = Sprite.doll_right2;
        this.right2 = Sprite.doll_right3;
        this.deadSprite = Sprite.doll_dead;

        movingAnimations[0] = this.left;
        movingAnimations[1] = this.left1;
        movingAnimations[2] = this.left2;
        this.setImg(this.left.getFxImage());
        this.setMovingSpeed(MIN_SPEED);
    }

    @Override
    protected MOVING_DIRECTION getBestDirection() {
        return getRandomDirection();
    }
}

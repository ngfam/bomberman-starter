package uet.oop.bomberman.entities.enemies;

import javafx.scene.image.Image;
import uet.oop.bomberman.Map;
import uet.oop.bomberman.entities.abstracts.AutoMovingEnemy;
import uet.oop.bomberman.graphics.Sprite;
import uet.oop.bomberman.helpers.Lib;

import java.util.ArrayList;
import java.util.Random;

public class Kondoria extends AutoMovingEnemy {
    int currentStrategy;
    int strategyChangeCountdown = 0;
    private static final int BEFORE_CHANGE_STRATEGY = 100;

    public Kondoria(int xUnit, int yUnit, Image img) {
        super(xUnit, yUnit, img, 1, 1);
        this.left = Sprite.kondoria_left1;
        this.left1 = Sprite.kondoria_left2;
        this.left2 = Sprite.kondoria_left3;
        this.right = Sprite.kondoria_right1;
        this.right1 = Sprite.kondoria_right2;
        this.right2 = Sprite.kondoria_right3;
        this.deadSprite = Sprite.kondoria_dead;
        movingAnimations[0] = this.left;
        movingAnimations[1] = this.left1;
        movingAnimations[2] = this.left2;
        this.setMovingSpeed(MIN_SPEED);
        this.setImg(this.left.getFxImage());
    }

    @Override
    protected boolean moveAble(int x, int y) {
        return x >= Sprite.SCALED_SIZE && x < Sprite.SCALED_SIZE * (Map.HEIGHT - 1) && y >= Sprite.SCALED_SIZE && y < Sprite.SCALED_SIZE * (Map.WIDTH - 1);
    }

    @Override
    protected MOVING_DIRECTION getBestDirection() {
        if (strategyChangeCountdown == 0) {
            currentStrategy = Math.abs(random.nextInt() % 3);
            strategyChangeCountdown = BEFORE_CHANGE_STRATEGY;
        } else {
            strategyChangeCountdown--;
        }

        if (currentStrategy < 2) {
            return getRandomDirection();
        } else {
            int bestDistance = Integer.MAX_VALUE;
            MOVING_DIRECTION bestDirection = MOVING_DIRECTION.STAND;
            ArrayList<MOVING_DIRECTION> directions = getMovableDirections();
            for(MOVING_DIRECTION direction: directions) {
                int nx = getNewX(x, direction);
                int ny = getNewY(y, direction);
                if (Lib.distanceToBomberman(nx, ny) < bestDistance) {
                    bestDistance = Lib.distanceToBomberman(nx, ny);
                    bestDirection = direction;
                }
            }
            return bestDirection;
        }
    }
}

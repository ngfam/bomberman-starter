package uet.oop.bomberman.entities.enemies;

import javafx.scene.image.Image;
import uet.oop.bomberman.entities.abstracts.AutoMovingEnemy;
import uet.oop.bomberman.graphics.Sprite;

import java.util.ArrayList;
import java.util.Random;

public class Balloon extends AutoMovingEnemy {
    public Balloon(int xUnit, int yUnit, Image img) {
        super(xUnit, yUnit, img, 1, 1);
        this.left = Sprite.balloom_left1;
        this.left1 = Sprite.balloom_left2;
        this.left2 = Sprite.balloom_left3;
        this.right = Sprite.balloom_right1;
        this.right1 = Sprite.balloom_right2;
        this.right2 = Sprite.balloom_right3;
        this.deadSprite = Sprite.balloom_dead;

        movingAnimations[0] = this.left;
        movingAnimations[1] = this.left1;
        movingAnimations[2] = this.left2;

        this.setMovingSpeed(MIN_SPEED);
        this.setImg(this.left.getFxImage());
    }

    @Override
    protected MOVING_DIRECTION getBestDirection() {
        return getRandomDirection();
    }
}

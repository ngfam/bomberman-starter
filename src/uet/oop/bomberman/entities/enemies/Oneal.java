package uet.oop.bomberman.entities.enemies;

import javafx.scene.image.Image;
import uet.oop.bomberman.entities.abstracts.AutoMovingEnemy;
import uet.oop.bomberman.graphics.Sprite;
import uet.oop.bomberman.helpers.Lib;

import java.util.ArrayList;
import java.util.Random;

public class Oneal extends AutoMovingEnemy {
    public Oneal(int xUnit, int yUnit, Image img) {
        super(xUnit, yUnit, img, 1, 3);
        this.left = Sprite.oneal_left1;
        this.left1 = Sprite.oneal_left2;
        this.left2 = Sprite.oneal_left3;
        this.right = Sprite.oneal_right1;
        this.right1 = Sprite.oneal_right2;
        this.right2 = Sprite.oneal_right3;
        this.deadSprite = Sprite.oneal_dead;
        movingAnimations[0] = this.left;
        movingAnimations[1] = this.left1;
        movingAnimations[2] = this.left2;
        this.setMovingSpeed(MIN_SPEED);
        this.setImg(this.left.getFxImage());
    }

    @Override
    protected MOVING_DIRECTION getBestDirection() {
        ArrayList<MOVING_DIRECTION> directions = getMovableDirections();
        MOVING_DIRECTION bfsDirection = Lib.getBestDirection(this.getGridX(), this.getGridY());
        if (bfsDirection != null && bfsDirection != MOVING_DIRECTION.STAND && directions.contains(bfsDirection)) {
            return bfsDirection;
        } else {
            return getRandomDirection();
        }
    }
}

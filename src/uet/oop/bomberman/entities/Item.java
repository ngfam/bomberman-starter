package uet.oop.bomberman.entities;

import uet.oop.bomberman.Container;
import uet.oop.bomberman.Map;
import uet.oop.bomberman.entities.abstracts.Entity;
import uet.oop.bomberman.entities.bomber.Bomber;
import uet.oop.bomberman.graphics.Sprite;

public class Item extends Entity {
    public enum ITEM_TYPE {
        BOMB_ITEM,
        FLAME_ITEM,
        SPEED_ITEM,
        PORTAL_ITEM
    }

    private boolean eaten;
    private final ITEM_TYPE type;

    public Item(int xUnit, int yUnit, ITEM_TYPE type) {
        super(xUnit, yUnit, null);
        switch (type) {
            case BOMB_ITEM -> {
                this.setImg(Sprite.powerup_bombs.getFxImage());
            }
            case FLAME_ITEM -> {
                this.setImg(Sprite.powerup_flames.getFxImage());
            }
            case SPEED_ITEM -> {
                this.setImg(Sprite.powerup_speed.getFxImage());
            }
            case PORTAL_ITEM -> {
                this.setImg(Sprite.portal.getFxImage());
            }
        }
        this.type = type;
    }

    @Override
    public void update() {
        if (this.isEaten()) return;
        Bomber player = Container.bomberman;
        if (player.getGridX() == this.getGridX() && player.getGridY() == this.getGridY()) {
            switch (type) {
                case BOMB_ITEM -> player.increaseBombLimit();
                case FLAME_ITEM -> player.increaseBombLength();
                case SPEED_ITEM -> player.increaseSpeed();
                case PORTAL_ITEM -> {
                    if (Container.enemies.isEmpty()) Map.levelUp();
                    else return;
                }
            }
            this.setEaten(true);
            this.setImg(null);
        }
    }

    public boolean isEaten() {
        return eaten;
    }

    public void setEaten(boolean eaten) {
        assert !this.eaten && eaten;
        this.eaten = true;
    }
}

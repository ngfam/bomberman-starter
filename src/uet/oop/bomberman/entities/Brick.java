package uet.oop.bomberman.entities;

import javafx.scene.image.Image;
import uet.oop.bomberman.Map;
import uet.oop.bomberman.entities.abstracts.Entity;
import uet.oop.bomberman.graphics.Sprite;

public class Brick extends Entity {
    boolean destroyed;
    int destroyedAnimationSteps;

    public Brick(int xUnit, int yUnit, Image img) {
        super(xUnit, yUnit, img);
        destroyed = false;
    }

    @Override
    public void update() {
        updateImage();
    }

    @Override
    public void updateImage() {
        if (isFullyDestroyed()) return;
        if (isDestroyed()) {
            this.setImg(
                Sprite.movingSprite(
                    Sprite.brick_exploded,
                    Sprite.brick_exploded1,
                    Sprite.brick_exploded2,
                    destroyedAnimationSteps,
                    Sprite.ANIMATING_CYCLE
                ).getFxImage()
            );
            destroyedAnimationSteps--;
            if (destroyedAnimationSteps == 0) {
                Map.setObjectAt(this.getGridX(), this.getGridY(), new Grass(this.getGridX(), this.getGridY(), Sprite.grass.getFxImage()));
            }
        }
    }

    public boolean isDestroyed() {
        return destroyed;
    }

    public void setDestroyed(boolean destroyed) {
        if (this.isDestroyed()) {
            return;
        }
        this.destroyed = destroyed;
        if (destroyed) {
            setDestroyedAnimationSteps(Sprite.ANIMATING_CYCLE);
        }
    }

    public int getDestroyedAnimationSteps() {
        return destroyedAnimationSteps;
    }

    public void setDestroyedAnimationSteps(int destroyedAnimationSteps) {
        this.destroyedAnimationSteps = destroyedAnimationSteps;
    }

    public boolean isFullyDestroyed() {
        return isDestroyed() && destroyedAnimationSteps == 0;
    }
}

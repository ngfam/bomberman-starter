package uet.oop.bomberman.entities.abstracts;

import javafx.scene.image.Image;
import uet.oop.bomberman.graphics.Sprite;

import java.util.ArrayList;
import java.util.Random;

public abstract class AutoMovingEnemy extends MovingEntity {
    protected int MAX_SPEED;
    protected int MIN_SPEED;

    protected static final int BEFORE_CHANGE_SPEED = 50;
    protected int speedChangeCountdown = 0;
    private static final int BEFORE_CHANGE_DIRECTION = 100;
    private int directionChangeCountdown;
    protected static final Random random = new Random();

    protected Sprite left;
    protected Sprite left1;
    protected Sprite left2;

    protected Sprite right;
    protected Sprite right1;
    protected Sprite right2;

    protected Sprite[] movingAnimations = new Sprite[3];

    protected Sprite deadSprite;

    public AutoMovingEnemy(int xUnit, int yUnit, Image img, int MIN_SPEED, int MAX_SPEED) {
        super(xUnit, yUnit, img);
        this.setMoving(true);
        this.MAX_SPEED = MAX_SPEED;
        this.MIN_SPEED = MIN_SPEED;
    }

    @Override
    public void update() {
        if (checkTouchingExplosion()) {
            setDead(true);
        }
        if (!isDead()) {
            updateSpeed();

            setMovingDirection(getBestDirection());
            updateImage();
        }
        animate();
    }

    @Override
    public void updateImage() {
        if (movingDirection == MOVING_DIRECTION.LEFT) {
            movingAnimations[0] = left;
            movingAnimations[1] = left1;
            movingAnimations[2] = left2;
        } else if (movingDirection == MOVING_DIRECTION.RIGHT) {
            movingAnimations[0] = right;
            movingAnimations[1] = right1;
            movingAnimations[2] = right2;
        }
        this.setImg(
                Sprite.movingSprite(
                    movingAnimations[0],
                    movingAnimations[1],
                    movingAnimations[2],
                    getAnimatingStep(),
                    Sprite.ANIMATING_CYCLE * 3
                ).getFxImage()
        );
        switch (movingDirection) {
            case UP -> this.moveUp();
            case DOWN -> this.moveDown();
            case LEFT -> this.moveLeft();
            case RIGHT -> this.moveRight();
        }
    }

    protected ArrayList<MOVING_DIRECTION> getMovableDirections() {
        ArrayList<MOVING_DIRECTION> directions = new ArrayList<>();
        directions.add(MOVING_DIRECTION.STAND);
        if (moveAble(x, y - movingSpeed)) directions.add(MOVING_DIRECTION.UP);
        if (moveAble(x, y + movingSpeed)) directions.add(MOVING_DIRECTION.DOWN);
        if (moveAble(x - movingSpeed, y)) directions.add(MOVING_DIRECTION.LEFT);
        if (moveAble(x + movingSpeed, y)) directions.add(MOVING_DIRECTION.RIGHT);
        return directions;
    }

    protected void updateSpeed() {
        if (speedChangeCountdown != 0) {
            speedChangeCountdown--;
            return;
        }
        int chance = MIN_SPEED + Math.abs(random.nextInt()) % (MAX_SPEED - MIN_SPEED + 1) + 1;
        if (chance < this.getMovingSpeed()) {
            this.setMovingSpeed(this.getMovingSpeed() - 1);
        } else if (chance > this.getMovingSpeed()) {
            this.setMovingSpeed(this.getMovingSpeed() + 1);
        }
        speedChangeCountdown = BEFORE_CHANGE_SPEED;
    }

    @Override
    public void setDead(boolean dead) {
        assert !this.dead && dead;
        this.dead = true ;
        dyingCountdown = 40;
        this.setImg(this.deadSprite.getFxImage());
    }

    protected abstract MOVING_DIRECTION getBestDirection();

    protected MOVING_DIRECTION getRandomDirection() {
        ArrayList<MOVING_DIRECTION> directions = getMovableDirections();
        if (movingDirection != MOVING_DIRECTION.STAND && directions.contains(movingDirection) && directionChangeCountdown > 0) {
            directionChangeCountdown--;
            return movingDirection;
        } else {
            directionChangeCountdown = BEFORE_CHANGE_DIRECTION;
            return directions.get(Math.abs(random.nextInt()) % directions.size());
        }
    }
}

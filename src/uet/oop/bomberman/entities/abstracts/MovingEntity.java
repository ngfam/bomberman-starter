package uet.oop.bomberman.entities.abstracts;

import javafx.scene.image.Image;
import uet.oop.bomberman.Map;

public abstract class MovingEntity extends Entity {
    public static enum MOVING_DIRECTION {
        STAND,
        LEFT,
        RIGHT,
        UP,
        DOWN
    };

    protected int dyingCountdown;
    protected boolean dead;

    protected int movingSpeed;
    protected boolean isMoving;
    protected MOVING_DIRECTION movingDirection;

    protected int animatingStep;
    public final static int MAX_STEP = 10000;
    public final static int ANIMATING_CYCLE = 20;

    public MovingEntity(int xUnit, int yUnit, Image img) {
        super(xUnit, yUnit, img);
        movingDirection = MOVING_DIRECTION.STAND;
        movingSpeed = 4;
    }

    protected boolean moveAble(int x, int y) {
        return Map.moveAble(x, y);
    }

    public void moveLeft() {
        if (!isMoving() || !moveAble(x - movingSpeed, y)) return;
        this.setX(x - movingSpeed);
    }

    public void moveRight() {
        if (!isMoving() || !moveAble(x + movingSpeed, y)) return;
        this.setX(x + movingSpeed);
    }

    public void moveUp() {
        if (!isMoving() || !moveAble(x, y - movingSpeed)) return;
        this.setY(y - movingSpeed);
    }


    public void moveDown() {
        if (!isMoving() || !moveAble(x, y + movingSpeed)) return;
        this.setY(y + movingSpeed);
    }

    public MOVING_DIRECTION getMovingDirection() {
        return movingDirection;
    }

    public void setMovingDirection(MOVING_DIRECTION movingDirection) {
        this.movingDirection = movingDirection;
    }

    public boolean isMoving() {
        return isMoving;
    }

    public void setMoving(boolean moving) {
        isMoving = moving;
    }

    public int getMovingSpeed() {
        return movingSpeed;
    }

    public void setMovingSpeed(int movingSpeed) {
        this.movingSpeed = movingSpeed;
    }

    public int getAnimatingStep() {
        return animatingStep;
    }

    public void setAnimatingStep(int animatingStep) {
        this.animatingStep = animatingStep;
    }

    public void animate() {
        if (this.animatingStep == MAX_STEP) {
            this.animatingStep = 0;
        } else {
            this.animatingStep = this.animatingStep + 1;
        }
        if (dyingCountdown > 0) dyingCountdown--;
    }

    public boolean isFullyDead() {
        return isDead() && dyingCountdown == 0;
    }

    public boolean isDead() {
        return dead;
    }

    public abstract void setDead(boolean dead);

    public static int getNewX(int _x, MOVING_DIRECTION direction, int speed) {
        switch (direction) {
            case RIGHT -> _x += speed;
            case LEFT -> _x -= speed;
        }
        return _x;
    }

    public static int getNewY(int _y, MOVING_DIRECTION direction, int speed) {
        switch (direction) {
            case UP -> _y -= speed;
            case DOWN -> _y += speed;
        }
        return _y;
    }

    protected int getNewX(int _x, MOVING_DIRECTION direction) {
        return getNewX(_x, direction, this.getMovingSpeed());
    }

    protected int getNewY(int _y, MOVING_DIRECTION direction) {
        return getNewY(_y, direction, this.getMovingSpeed());
    }
}

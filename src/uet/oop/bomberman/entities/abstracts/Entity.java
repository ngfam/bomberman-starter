package uet.oop.bomberman.entities.abstracts;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import uet.oop.bomberman.Container;
import uet.oop.bomberman.entities.bomber.Bomb;
import uet.oop.bomberman.entities.bomber.Explosion;
import uet.oop.bomberman.graphics.Sprite;
import uet.oop.bomberman.helpers.Lib;

public abstract class Entity {
    //Tọa độ X tính từ góc trái trên trong Canvas
    protected int x;

    //Tọa độ Y tính từ góc trái trên trong Canvas
    protected int y;

    protected Image img;

    //Khởi tạo đối tượng, chuyển từ tọa độ đơn vị sang tọa độ trong canvas
    public Entity( int xUnit, int yUnit, Image img) {
        this.x = xUnit * Sprite.SCALED_SIZE;
        this.y = yUnit * Sprite.SCALED_SIZE;
        this.img = img;
    }

    public void render(GraphicsContext gc) {
        gc.drawImage(img, x, y);
    }
    public abstract void update();

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Image getImg() {
        return img;
    }

    public void setImg(Image img) {
        this.img = img;
    }

    public void updateImage() {

    }

    public int getGridX() {
        return (this.x + Sprite.SCALED_SIZE / 2) / Sprite.SCALED_SIZE;
    }

    public int getGridY() {
        return (this.y + Sprite.SCALED_SIZE / 2) / Sprite.SCALED_SIZE;
    }

    public boolean checkTouchingExplosion() {
        for (Bomb bomb: Container.bombs) {
            if (bomb.isExploded() && bomb.isTouching(this)) return true;
        }
        for (Explosion explosion: Container.explosions) {
            if (explosion.isTouching(this)) return true;
        }
        return false;
    }

    public boolean isTouching(Entity e) {
        return Lib.isTouching(this.getX(), this.getY(), e.getX(), e.getY());
    }
}
